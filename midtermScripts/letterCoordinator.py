
import csv


class letterCoordinator(object):
    """Class for reading saved coordinates for each letter and scaling them into the given taskspace"""
    # Coordinates from the CSV are expected to be in the range 0 <= x <= letterWidth and 0 <= y <= letterHeight
    letterWidth = 1.0
    letterHeight = 1.5

    def __init__(self, leftbound, rightbound, topbound, bottombound, xValue = 0.4):
        # Save the given task space
        self.leftbound = leftbound
        self.rightbound = rightbound
        self.topbound = topbound
        self.bottombound = bottombound
        self.xValue = float(xValue)
    
    def coordinateIsValid(self, coord):
        # make sure the given list has two entries within the ranges for y and z
        if not len(coord) == 2: return False
        (y,z) = (coord[0], coord[1])
        return (0 <= y <= self.letterWidth) and (0 <= z <= self.letterHeight)
    
    def scaleCoordinate(self, coord):
        # scale coordinate to whatever dimensions were given in the initializer
        (y,z) = (coord[0], coord[1])
        newy = (y/self.letterWidth)*(self.rightbound - self.leftbound) + self.leftbound
        newz = (z/self.letterHeight)*(self.topbound - self.bottombound) + self.bottombound
        return [newy, newz]
    
    def listToCoord(self, strlist):
        # turn the string values from the csv into floats
        return [float(strlist[0]), float(strlist[1])]


    def readCoordinateList(self, filename):
        # read list of coordinates from the given csv file
        with open(filename) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            coords = []
            for row in csv_reader:
                print(row)
                line_count += 1
                coord = self.listToCoord(row)
                if self.coordinateIsValid(coord): coords.append([self.xValue] + self.scaleCoordinate(coord))
            print(f'Processed {line_count} lines.')
            return coords
    
    def B(self):
        # return the list of x,y,z coordinates needed to draw the letter B
        BFilename = "letterB.csv"
        return self.readCoordinateList(BFilename)
    
    def M(self):
        # return the list of x,y,z coordinates needed to draw the letter M
        MFilename = "letterM.csv"
        return self.readCoordinateList(MFilename)
    
    def W(self):
        # return the list of x,y,z coordinates needed to draw the letter W
        WFilename = "letterW.csv"
        return self.readCoordinateList(WFilename)
    
    def rect(self):
        # return the list of x,y,z coordinates needed to draw a parimeter of the given taskspace
        RectFilename = "letterRect.csv"
        return self.readCoordinateList(RectFilename)


if __name__ == "__main__":
    # test program prints out list of coordinates
    lc = letterCoordinator(0.0, 0.1, 0.0, 0.1, 0.4)
    print(lc.B())

