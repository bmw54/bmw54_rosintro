#!/usr/bin/bash

pi=3.14159265358979
twoPi=$(echo "scale=14 ; $pi*2.0" | bc)
halfPi=$(echo "scale=14 ; $pi/2.0" | bc)
threeHalfPi=$(echo "scale=14 ; $halfPi*3.0" | bc)

rosservice call /reset #resets turtle to center, pointing right, clears pen
rosservice call /kill turtle1 #deletes default turtle

rosservice call /spawn 1 1 $halfPi turtle1 #x y theta name

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- "[4.0, 0.0, 0.0]" '[0.0, 0.0, 0.0]'
rosservice call /turtle1/teleport_relative 0 $threeHalfPi
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- "[1.0, 0.0, 0.0]" "[0.0, 0.0, 0.0]"
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- "[$pi, 0.0, 0.0]" "[0.0, 0.0, -$pi]"
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- "[-$pi, 0.0, 0.0]" "[0.0, 0.0, -$pi]"
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
 	-- "[-1.0, 0.0, 0.0]" "[0.0, 0.0, 0.0]"
